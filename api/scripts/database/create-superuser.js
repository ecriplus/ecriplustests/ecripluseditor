#! /usr/bin/env node
/* eslint no-console: ["off"] */

const { knex } = require('../../db/knex-database-connection');
const util = require('util');

function usage() {
  console.log('Usage: ', 'init-db <name> <trigram> <apiKey>');
}

function insertOrUpdate(tableName, rows){
  return knex.transaction(trx => {
      let queries = rows.map(tuple =>
        trx.raw(util.format('%s ON CONFLICT (id) DO UPDATE SET %s',
          trx(tableName).insert(tuple).toString().toString(),
          trx(tableName).update(tuple).whereRaw(`users.id = '${tuple.id}'`).toString().replace(/^update\s.*\sset\s/i, '')
        ))               
        .transacting(trx)
      );
      return Promise.all(queries).then(trx.commit).catch(trx.rollback);
  })
}

function initDb(name, trigram, apiKey) {
  
  const userValues = [{
    id:1,
    name,
    trigram,
    apiKey,
    access: "admin",
    createdAt: new Date(),
    updatedAt: new Date()
  }];

  return insertOrUpdate('users', userValues);
}

if (require.main === module) {

  if (process.argv.length != 5) {
    console.error('Wrong number of arguments.');
    usage();
    knex.destroy();
    process.exit(1);
  }

  const [ name, trigram, apiKey] = process.argv.slice(2);

  // Create User
  console.log(`Create Superuser ${name}.`);
  initDb(name, trigram, apiKey)
    .catch((e) => {
      console.error('Error creating Superuser:', e.message);
      knex.destroy();
      process.exit(1);
    })
    .then(()=>{
      console.log(`Superuser ${name} created.`);
      knex.destroy();
    });
}
