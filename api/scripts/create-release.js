#! /usr/bin/env node
/* eslint no-console: ["off"] */

const { queue: createReleaseQueue } = require('../lib/infrastructure/scheduled-jobs/release-job');

async function createRelease() {
  createReleaseQueue.add({ slackNotification: false })
    .catch((e) => {
      console.error('Error creating Release:', e.message);
      process.exit(1);
    })
    .then(() => {
      console.log(`Release created.`);
      process.exit(0);
    })
}

createRelease();