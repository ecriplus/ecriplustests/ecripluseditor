module.exports = class Tutorial {
  constructor({
    // attributes
    id,
    duration,
    format,
    link,
    source,
    title,
    locale,
    // includes
    // references
  } = {}) {
    // attributes
    this.id = id;
    this.duration = duration;
    this.format = format;
    this.link = link;
    this.source = source;
    this.title = title;
    this.locale = locale;
    // includes
    // references
  }
};
